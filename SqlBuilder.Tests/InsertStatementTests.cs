using SqlBuilder.Statements;
using Xunit;

namespace SqlBuilder.Tests
{
    public class InsertStatementTests
    {
        [Fact]
        public void TestSimple()
        {
            var sql = new InsertStatement()
                .Into("Users")
                .Values("'Test'", "'User'", "27");

            const string expected = "insert into Users values ('Test', 'User', 27)";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestWithColumns()
        {
            var sql = new InsertStatement()
                .Into("Users")
                .Cols("FirstName", "LastName", "Age")
                .Values("'Test'", "'User'", "27");

            const string expected = "insert into Users (FirstName, LastName, Age) values ('Test', 'User', 27)";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestDefaultValues()
        {
            var sql = new InsertStatement()
                .Into("Users");

            const string expected = "insert into Users default values";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestMultipleValues()
        {
            var sql = new InsertStatement()
                .Into("Users")
                .Values("'Test'")
                .Values("'Test 2'");

            const string expected = "insert into Users values ('Test'), ('Test 2')";

            Assert.Equal(expected, sql);
        }
    }
}
