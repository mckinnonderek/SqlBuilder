using SqlBuilder.Statements;
using Xunit;

namespace SqlBuilder.Tests
{
    public class SelectStatementTests
    {
        [Fact]
        public void TestSimple()
        {
            var sql = new SelectStatement().From("Users");

            const string expected = "select * from Users";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestColumns()
        {
            var sql = new SelectStatement()
                .Cols("Id", "FirstName")
                .From("Users");

            const string expected = "select Id, FirstName from Users";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestTop()
        {
            var sql = new SelectStatement().Top(10).From("Users");

            const string expected = "select top 10 * from Users";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestJoin()
        {
            var sql = new SelectStatement()
                .From("Users u")
                .Join("Comments c", "c.UserId = u.Id");

            const string expected = "select * from Users u inner join Comments c on c.UserId = u.Id";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestMultipleJoin()
        {
            var sql = new SelectStatement()
                .From("Users u")
                .Join("Comments c", "c.UserId = u.Id")
                .Join("Likes l", "l.CommentId = c.Id", "left");

            const string expected = "select * from Users u inner join Comments c on c.UserId = u.Id left join Likes l on l.CommentId = c.Id";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestWhere()
        {
            var sql = new SelectStatement()
                .Cols("a", "b", "c")
                .From("test")
                .Where("a > b");

            const string expected = "select a, b, c from test where a > b";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestConditionalWhere()
        {
            var sql = new SelectStatement()
                .Cols("a", "b", "c")
                .From("test")
                .Where(false, "a > b");

            var expected = "select a, b, c from test";

            Assert.Equal(expected, sql);

            sql.Where(true, "a > b");

            expected = "select a, b, c from test where a > b";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestMultipleWhere()
        {
            var sql = new SelectStatement()
                .Cols("a", "b", "c")
                .From("test")
                .Where("a > b")
                .Where("a > c");

            const string expected = "select a, b, c from test where a > b and a > c";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestSubSelect()
        {
            var sql = new SelectStatement()
                .From("users")
                .Where("Id in", new SelectStatement().Cols("Id").From("banned_users"));

            const string expected = "select * from users where Id in (select Id from banned_users)";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestOrderBy()
        {
            var sql = new SelectStatement()
                .Cols("a")
                .From("test")
                .OrderBy("a");

            var expected = "select a from test order by a";

            Assert.Equal(expected, sql.ToString());

            sql = new SelectStatement()
                .Cols("a")
                .From("test")
                .OrderBy("a", descending: true);

            expected = "select a from test order by a desc";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestGroupBy()
        {
            var sql = new SelectStatement()
                .From("Users")
                .GroupBy("Age");

            const string expected = "select * from Users group by Age";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestHaving()
        {
            var sql = new SelectStatement()
                .Cols("count(Id)", "Country")
                .From("Customers")
                .GroupBy("Country")
                .Having("count(Id) > 5");

            const string expected = "select count(Id), Country from Customers group by Country having count(Id) > 5";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestMultipleOrderBy()
        {
            var sql = new SelectStatement()
                .From("test")
                .OrderBy("a")
                .OrderBy("b");

            const string expected = "select * from test order by a, b";

            Assert.Equal(expected, sql);
        }
    }
}