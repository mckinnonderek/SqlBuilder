using SqlBuilder.Statements;
using Xunit;

namespace SqlBuilder.Tests
{
    public class UpdateStatementTests
    {
        [Fact]
        public void TestSimple()
        {
            var sql = new UpdateStatement("Users")
                .Set("Enabled", "1");

            var expected = "update Users set Enabled = 1";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestWhere()
        {
            var sql = new UpdateStatement("Users")
                .Set("Age", "28")
                .Where("FirstName = 'Derek'");

            var expected = "update Users set Age = 28 where FirstName = 'Derek'";

            Assert.Equal(expected, sql);
        }
    }
}
