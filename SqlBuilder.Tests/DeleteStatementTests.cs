using SqlBuilder.Statements;
using Xunit;

namespace SqlBuilder.Tests
{
    public class DeleteStatementTests
    {
        [Fact]
        public void TestSimple()
        {
            var sql = new DeleteStatement("Users");

            var expected = "delete from Users";

            Assert.Equal(expected, sql);
        }

        [Fact]
        public void TestWhere()
        {
            var sql = new DeleteStatement("Users")
                .Where("Age > 27");

            var expected = "delete from Users where Age > 27";

            Assert.Equal(expected, sql);
        }
    }
}
