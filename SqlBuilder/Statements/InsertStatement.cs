using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlBuilder.Statements
{
    public class InsertStatement
    {
        private string _table;
        private readonly List<string> _columns = new List<string>();
        private readonly List<IEnumerable<string>> _values = new List<IEnumerable<string>>();

        public static implicit operator string(InsertStatement statement) => statement.ToString();

        public InsertStatement Into(string table)
        {
            _table = table;
            return this;
        }

        public InsertStatement Cols(params string[] columns)
        {
            _columns.AddRange(columns);
            return this;
        }

        public InsertStatement Values(params string[] values)
        {
            _values.Add(values);
            return this;
        }

        public override string ToString()
        {
            var builder = new StringBuilder($"insert into {_table}");

            if (_columns.Any())
            {
                builder.Append($" ({string.Join(", ", _columns)})");
            }

            if (_values.Any())
            {
                var sets = _values.Select(set => $"({string.Join(", ", set)})");

                builder.Append($" values {string.Join(", ", sets)}");
            }
            else
            {
                builder.Append(" default values");
            }

            return builder.ToString();
        }
    }
}
