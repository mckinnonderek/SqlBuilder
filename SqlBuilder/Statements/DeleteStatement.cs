using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlBuilder.Statements
{
    public class DeleteStatement
    {
        private readonly string _table;
        private readonly List<string> _wheres = new List<string>();

        public static implicit operator string(DeleteStatement statement) => statement.ToString();

        public DeleteStatement(string table) => _table = table;

        public DeleteStatement Where(string where)
        {
            _wheres.Add(where);
            return this;
        }

        public override string ToString()
        {
            var builder = new StringBuilder($"delete from {_table}");

            if (_wheres.Any())
            {
                builder.Append($" where {string.Join(" and ", _wheres)}");
            }

            return builder.ToString();
        }
    }
}
