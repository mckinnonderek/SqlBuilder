using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlBuilder.Statements
{
    public class SelectStatement
    {
        private int? _top;
        private readonly List<string> _columns = new List<string>();
        private readonly List<string> _tables = new List<string>();
        private readonly List<(string, string, string)> _joins = new List<(string, string, string)>();
        private readonly List<string> _wheres = new List<string>();
        private readonly List<string> _orders = new List<string>();
        private readonly List<string> _groups = new List<string>();
        private readonly List<string> _havings = new List<string>();

        public static implicit operator string(SelectStatement statement) => statement.ToString();

        public SelectStatement Top(int top)
        {
            _top = top;
            return this;
        }

        public SelectStatement Cols(params string[] columns)
        {
            _columns.AddRange(columns);
            return this;
        }

        public SelectStatement From(params string[] tables)
        {
            _tables.AddRange(tables);
            return this;
        }

        public SelectStatement Join(string table, string onClause, string type = "inner")
        {
            _joins.Add((table, onClause, type));
            return this;
        }

        public SelectStatement Where(string where, SelectStatement subSelect = null)
        {
            if (subSelect != null)
            {
                where += $" ({subSelect})";
            }

            _wheres.Add(where);
            return this;
        }

        public SelectStatement Where(bool test, string where, SelectStatement subSelect = null)
        {
            return test ? Where(where, subSelect) : this;
        }

        public SelectStatement OrderBy(string column, bool descending = false)
        {
            _orders.Add($"{column}{(descending ? " desc" : string.Empty)}");
            return this;
        }

        public SelectStatement GroupBy(string column)
        {
            _groups.Add(column);
            return this;
        }

        public SelectStatement Having(string clause)
        {
            _havings.Add(clause);
            return this;
        }

        public override string ToString()
        {
            var builder = new StringBuilder("select");

            if (_top.HasValue)
            {
                builder.Append($" top {_top}");
            }

            if (!_columns.Any())
            {
                _columns.Add("*");
            }

            builder.Append($" {string.Join(", ", _columns)}");

            builder.Append($" from {string.Join(", ", _tables)}");

            if (_joins.Any())
            {
                foreach (var (table, onClause, type) in _joins)
                {
                    builder.Append($" {type} join {table} on {onClause}");
                }
            }

            if (_wheres.Any())
            {
                builder.Append($" where {string.Join(" and ", _wheres)}");
            }

            if (_groups.Any())
            {
                builder.Append($" group by {string.Join(", ", _groups)}");
            }

            if (_havings.Any())
            {
                builder.Append($" having {string.Join(" and ", _havings)}");
            }

            if (_orders.Any())
            {
                builder.Append($" order by {string.Join(", ", _orders)}");
            }

            return builder.ToString();
        }
    }
}