using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlBuilder.Statements
{
    public class UpdateStatement
    {
        private readonly string _table;
        private readonly List<(string column, string value)> _sets = new List<(string, string)>();
        private readonly List<string> _wheres = new List<string>();

        public static implicit operator string(UpdateStatement statement) => statement.ToString();

        public UpdateStatement(string table) => _table = table;

        public UpdateStatement Set(string column, string value)
        {
            _sets.Add((column, value));
            return this;
        }

        public UpdateStatement Where(string where)
        {
            _wheres.Add(where);
            return this;
        }

        public override string ToString()
        {
            var builder = new StringBuilder($"update {_table} ");

            var sets = _sets.Select(cv => $"{cv.column} = {cv.value}");
            builder.Append($"set {string.Join(", ", sets)}");

            if (_wheres.Any())
            {
                builder.Append($" where {string.Join(" and ", _wheres)}");
            }

            return builder.ToString();
        }
    }
}
