using SqlBuilder.Statements;
using System.Diagnostics.CodeAnalysis;

namespace SqlBuilder
{
    [ExcludeFromCodeCoverage]
    public static class Builder
    {
        public static SelectStatement Select(params string[] columns)
        {
            return new SelectStatement().Cols(columns);
        }

        public static InsertStatement Insert()
        {
            return new InsertStatement();
        }

        public static UpdateStatement Update(string table)
        {
            return new UpdateStatement(table);
        }

        public static DeleteStatement Delete(string table)
        {
            return new DeleteStatement(table);
        }
    }
}
